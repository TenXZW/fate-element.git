# 动态表单

::: tip
本表单组件采用schema小组件方式渲染表单，支持使用栅格模式自定义表单布局。我们可以做到一切您能想到的事，如果没有，请[提issue](https://gitee.com/TenXZW/fate-element/issues)。
:::

## 行内表单

- 我们只需要启用`inline`属性即可实现行内表单。
  <preview path="./InlineForm.vue"></preview>

## 栅格表单

- 在非`inline`模式下，我们可以通过设置每个`schema`下的字段的`layout`属性里面的`span`、`offset`、`xs`、`sm`、`md`、`lg`、`xl`
  属性来设置栅栏布局。
  <preview path="./GridForm.vue"></preview>

## 各类表单元素

- 下面会演示各类表单元素的渲染。默认为`input`元素，ui参数的widget参数可以传入`textarea`、`select`、`checkbox`、`radio`、
  `switch`、`date`、`time`、`datetime`、`upload`等各类element-plus的组件。

::: tip
`properties`字段里面可以定义每个表单的字段名（key），`ui`字段里的`props`字段里面可以传入各类element-plus组件的参数。
:::
<preview path="./AnyForm.vue"></preview>

## 表单API

--------------------------------------------------------------------------------

#### FFormProps 属性

| 属性名           | 说明                                                            | 类型                          | 默认值     |
|:--------------|:--------------------------------------------------------------|:----------------------------|:--------|
| inline        | 是否内联表单，默认为块级表单                                                | boolean                     | false   |
| schema        | 表单的架构定义，包括字段及其验证规则                                            | [FFSchema 属性](#ffschema-属性) | -       |
| model         | 表单数据模型，用于默认值数据绑定（不支持双向绑定）（和FFProperty的default同时存在时，优先default） | Record<string, any>         | -       |
| labelWidth    | 标签宽度，可以是数字表示像素值，或字符串表示其他单位                                    | number \| string            | '80px'  |
| labelPosition | 标签位置，可选值有左、右、顶                                                | 'left' \| 'right' \| 'top'  | 'right' |
| sureText      | 确定按钮文本内容，可能为空                                                 | string                      | '确认'    |
| cancelText    | 取消按钮文本内容，可能为空                                                 | string                      | '充值'    |
| sureIcon      | 确定按钮的图标，可以为空                                                  | string \| Component         | -       |
| cancelIcon    | 取消按钮的图标，可以为空                                                  | string \| Component         | -       |
| loading       | 表单是否加载中                                                       | boolean                     | false   |

<a name="ffschema-属性"></a>

#### FFSchema-属性

| 属性名          | 说明           | 类型                             | 默认值    |
|:-------------|:-------------|:-------------------------------|:-------|
| properties   | 字段属性定义       | [FFProperties](#ffproperty-属性) | -      |
| required     | 必填字段列表       | string[]                       | -      |
| gutter       | 表单项垂直间距，单位像素 | number                         | -      |
| elementWidth | 内联表单元素宽度     | number \| string               | '100%' |
| layout       | 表单项布局配置      | [FFLayout](#fflayout-属性)       | -      |

<a name="ffproperty-属性"></a>

#### FFProperty-属性

| 属性名          | 说明           | 类型                       | 默认值    |
|:-------------|:-------------|:-------------------------|:-------|
| type         | 字段类型         | [FFType](#fftype-属性)     | -      |
| title        | 字段标题         | string                   | -      |
| description  | 字段描述         | string                   | -      |
| default      | 字段默认值        | any                      | -      |
| enums        | 字段枚举值列表      | [FFEnum[]](#ffenum-属性)   | -      |
| pattern      | 字段验证的正则表达式   | string                   | -      |
| format       | 字段格式，如电话、邮箱等 | [FFFormat](#ffformat-属性) | -      |
| ui           | 字段UI配置       | [FUI](#fui-属性)           | -      |
| iif          | 条件渲染字段       | boolean                  | false  |
| elementWidth | 表单元素宽度       | number \| string         | '100%' |
| layout       | 字段布局配置       | [FFLayout](#fflayout-属性) | -      |
| rules        | 表单项的规则       | [FFRules[]](#ffrules-属性) | -      |

<a name="ffrules-属性"></a>

#### FFRules-属性

| 属性名       | 说明         | 类型                                                 | 默认值   |
|:----------|:-----------|:---------------------------------------------------|:------|
| message   | 验证失败时显示的信息 | string                                             | -     |
| trigger   | 验证触发方式     | 'blur' \| 'change' \| [FFTrigger[]](#fftrigger-属性) | -     |
| required  | 是否为必填项     | boolean                                            | false |
| pattern   | 验证的正则表达式   | string                                             | -     |
| validator | 自定义验证器     | function                                           | -     |

<a name="fftrigger-属性"></a>

#### FFTrigger-属性

| 值      | 说明      |
|:-------|:--------|
| blur   | 失去焦点时触发 |
| change | 值改变时触发  |

<a name="fflayout-属性"></a>

#### FFLayout-属性

| 属性名    | 说明                   | 类型     | 默认值 |
|:-------|:---------------------|:-------|:----|
| span   | 栅格宽度                 | number | 24  |
| xs     | 响应式布局 - 小屏幕宽度小于576px | number | 24  |
| sm     | 响应式布局 - 屏幕宽度≥576px   | number | 12  |
| md     | 响应式布局 - 屏幕宽度≥768px   | number | 8   |
| lg     | 响应式布局 - 屏幕宽度≥992px   | number | 6   |
| xl     | 响应式布局 - 屏幕宽度≥1200px  | number | 4   |
| offset | 栅格偏移                 | number | 0   |

<a name="fftype-属性"></a>

#### FFType-属性

| 值         | 说明  |
|:----------|:----|
| string    | 字符串 |
| number    | 数字  |
| boolean   | 布尔值 |
| array     | 数组  |
| object    | 对象  |
| null      | 空值  |
| undefined | 未定义 |
| symbol    | 符号  |
| bigint    | 大整数 |
| function  | 函数  |

<a name="ffenum-属性"></a>

#### FFEnum-属性

| 属性名      | 说明    | 类型                     | 默认值   |
|:---------|:------|:-----------------------|:------|
| value    | 枚举值   | any                    | -     |
| label    | 枚举标签  | string                 | -     |
| disabled | 是否禁用  | boolean                | false |
| children | 子枚举选项 | [FFEnum[]](#ffenum-属性) | -     |

<a name="ffformat-属性"></a>

#### FFFormat-属性

| 值     | 说明   |
|:------|:-----|
| phone | 电话号码 |
| email | 邮箱   |

<a name="fui-属性"></a>

#### FUI-属性

| 属性名       | 说明        | 类型                              | 默认值 |
|:----------|:----------|:--------------------------------|:----|
| widget    | 字段使用的组件类型 | [FFWidget](#ffwidget-属性)        | -   |
| props     | 组件属性配置    | 多种element-plus表单元素类型            | -   |
| asyncData | 异步数据加载函数  | Promise<[FFEnum[]](#ffenum-属性)> | -   |

<a name="ffwidget-属性"></a>

#### FFWidget-属性

| 值             | 说明        |
|:--------------|:----------|
| checkbox      | 复选框       |
| cascader      | 级联选择器     |
| color         | 颜色选择器     |
| year          | 年份选择器     |
| years         | 年份范围选择器   |
| month         | 月份选择器     |
| months        | 月份范围选择器   |
| date          | 日期选择器     |
| dates         | 日期范围选择器   |
| datetime      | 日期时间选择器   |
| week          | 星期选择器     |
| datetimerange | 日期时间范围选择器 |
| daterange     | 日期范围选择器   |
| monthrange    | 月份范围选择器   |
| yearrange     | 年份范围选择器   |
| number        | 数字输入框     |
| radio         | 单选框       |
| rate          | 评分控件      |
| select        | 下拉选择框     |
| slider        | 滑动条       |
| switch        | 开关        |
| tree-select   | 树形选择器     |
| upload        | 文件上传控件    |

---

## 表单slot

| slot 名称 | 说明               |            作用域参数 |
|:--------|:-----------------|-----------------:|
| operate | 操作按钮插槽，用于自定义操作按钮 | onSubmit/onReset |

## 表单事件

| 事件名      | 说明         | 参数                       |
|:---------|:-----------|:-------------------------|
| onSubmit | 表单提交时触发    | data:Record<string, any> |
| onReset  | 表单重置时触发    | -                        |
| onChange | 表单值改变时触发   | data:Record<string, any> |
| onInput  | 表单项值改变时触发  | data:Record<string, any> |
| onFocus  | 表单项获取焦点时触发 | key:string               |
| onBlur   | 表单项失去焦点时触发 | key:string               |
| onClear  | 清除表单项时触发   | key:string               |
