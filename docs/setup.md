# 📦 安装与配置

以下步骤将指导你如何在 Vue 3 项目中安装并配置 **FateElement** 组件库，包括引入 **Element-Plus** 的配置。

---

## 1. 使用 pnpm 安装 FateElement 和 Element-Plus

我们推荐使用 **pnpm** 进行安装，以提升依赖管理的速度和效率：

```bash
# 安装 FateElement 和 Element-Plus
pnpm add fate-element element-plus
```

## 2.配置 FateElement 和 Element-Plus
在 *main.ts* 文件中引入并全局注册 *FateElement* 和 *Element-Plus*：
```typescript
import { createApp } from 'vue'
import App from './App.vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import FateElement from 'fate-element'
import 'fate-element/lib/fate-element.css'
const app = createApp(App)
app.use(ElementPlus)
app.use(FateElement)
app.mount('#app')
```

## 3. 配置webstorm提示
由于pnpm配合vite-plugin-dts打包的原因，webstrom无法直接识别到node_modules下的包里面的props类型，需要在`tsconfig.json`文件里手动配置下，后续官方出了解决方案之后会同步更新。
```json
{
  "compilerOptions": {
    "baseUrl": ".",
    "paths": {
      "fate-element": [
        "node_modules/fate-element"
      ]
    },
    "typeRoots": [
      "node_modules/@types",
      "fate-element"
    ]
  }
}
```
