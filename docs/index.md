---
layout: home

hero:
  name: "🌌 FateElement"
  text: "一个充满创意的 Vue 组件库站点"
  tagline: "基于 Element-Plus 的高效二次封装，尽享组件开发之乐！"
  actions:
    - theme: brand
      text: 开始探索
      link: /introduction
    - theme: alt
      text: API 文档
      link: /demo/form/dynamic-form

features:
  - title: 🧩 灵活的动态组件
    details: 动态表格和表单生成，让数据管理更简单。
  - title: ⚡️ 命令式弹窗服务
    details: 轻松触发对话框，为你的 UI 工作流赋能！
  - title: 🚀 现代化技术栈
    details: 基于 Vue 3、TypeScript 和 Vite 打造的超快速、可扩展组件库。
  - title: 🎨 完美融合 Element-Plus
    details: 在 Element-Plus 的基础上，提供更多灵活配置与功能扩展。
  - title: 🔧 易于集成与定制
    details: 轻松集成到你的项目中，为你的组件库注入新活力。
  - title: 💻 社区驱动的持续迭代
    details: 欢迎反馈与贡献，共同打造更强大的 FateElement！
---
