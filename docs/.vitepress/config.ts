import {defineConfig} from 'vitepress'
import {containerPreview, componentPreview} from '@vitepress-demo-preview/plugin'

// https://vitepress.dev/reference/site-config
export default defineConfig({
    title: "🌌 FateElement",
    description: "基于 Element-Plus 的个性化 Vue 组件库",
    themeConfig: {
        // https://vitepress.dev/reference/default-theme-config
        nav: [
            {text: '主页', link: '/'},
            {text: '入门', link: '/introduction'},
            {text: 'API 文档', link: '/demo/form/dynamic-form'}
        ],
        sidebar: [
            {
                text: '快速入门',
                items: [
                    {text: '项目介绍', link: '/introduction'},
                    {text: '安装与配置', link: '/setup'}
                ]
            },
            {
                text: '组件示例',
                items: [
                    {text: '动态表单', link: '/demo/form/dynamic-form'},
                    {text: '动态表格', link: '/demo/table/dynamic-table'}
                ]
            },
            {
                text: '进阶指南',
                items: [
                    {text: '自定义主题', link: '/custom-theme'},
                    {text: '优化与性能', link: '/performance'},
                    {text: '常见问题', link: '/faq'}
                ]
            }
        ],
        socialLinks: [
            {icon: 'github', link: 'https://gitee.com/TenXZW/fate-element'}
        ],
    },
    markdown: {
        config(md) {
            md.use(containerPreview)
            md.use(componentPreview)
        }
    }
})
