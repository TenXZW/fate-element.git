// https://vitepress.dev/guide/custom-theme
import {App, h} from 'vue'
import type {Theme} from 'vitepress'
import DefaultTheme from 'vitepress/theme'
import './style.css'
import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css';
import 'element-plus/theme-chalk/dark/css-vars.css'
import FateElement from 'fate-element'
import 'fate-element/lib/style.css'

import {ElementPlusContainer} from '@vitepress-demo-preview/component'
import '@vitepress-demo-preview/component/dist/style.css'


export default {
    extends: DefaultTheme,
    Layout: () => {
        return h(DefaultTheme.Layout, null, {
            // https://vitepress.dev/guide/extending-default-theme#layout-slots
        })
    },
    enhanceApp({app}: { app: App }) {
        app.component('demo-preview', ElementPlusContainer)
        app.use(ElementPlus)
        app.use(FateElement)
    }
} satisfies Theme
