import { App } from 'vue';
import FForm from '@fate/components/fate-form/FForm.vue';
import FTable from '@fate/components/fate-table/FTable.vue';
/**
 * 这里默认导出一个vue支持的install方法
 * 可以在main.ts中使用以下方式全局导入组件
 *
 * import FateElement from "fate-element"
 * .....
 * app.use(FateElement);
 */
export default {
  install(app: App) {
    app.component('FForm', FForm);
    app.component('FTable', FTable);
  }
};

// 导出组件
export { default as FForm } from '@fate/components/fate-form/FForm.vue';
export { default as FTable } from '@fate/components/fate-table/FTable.vue';

// 导出功能
export { useDialog } from '@fate/components/fate-dialog/useDialog';

// 导出类型
export type { FFSchema } from '@fate/components/fate-form/fform.types';
export * from '@fate/components/fate-form/fform.types';
export * from '@fate/components/fate-table/ftable.types';

/**
 * 这里是重点，需要将这些组件在ts中声明为全局组件；
 */
declare module 'vue' {
  export interface GlobalComponents {
    FForm: typeof FForm;
    FTable: typeof FTable;
  }
}
