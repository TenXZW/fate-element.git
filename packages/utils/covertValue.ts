import { FFType } from '../components/fate-form/fform.types';

/**
 *  Convert value to specified type
 * @param type 类型
 * @param value 值
 */
export function convertToType(type: FFType, value: any): any {
  try {
    switch (type) {
      case 'string':
        return String(value);
      case 'number': {
        const num = Number(value);
        return isNaN(num) ? value : num;
      }
      case 'boolean':
        return Boolean(value);
      case 'array':
        return Array.isArray(value) ? value : [value];
      case 'object':
        return typeof value === 'object' && value !== null ? value : { value };
      case 'null':
        return null;
      case 'undefined':
        return undefined;
      case 'symbol':
        return typeof value === 'symbol' ? value : Symbol(value);
      case 'bigint':
        try {
          return BigInt(value);
        } catch {
          return value;
        }
      case 'function':
        return typeof value === 'function' ? value : () => value;
      default:
        return value;
    }
  } catch {
    return value;
  }
}

/**
 * 将代理对象转换为普通对象。
 */
export function convertProxyToNormal(proxyObject: any): any {
  if (Array.isArray(proxyObject)) {
    return proxyObject.map((item) => {
      if (typeof item === 'object' && item !== null) {
        return convertProxyToNormal(item); // 递归处理子项
      }
      return item; // 非对象直接返回
    });
  }

  // 如果不是数组，认为是普通对象处理
  const normalObject: { [key: string]: any } = {};
  for (const key in proxyObject) {
    if (proxyObject.hasOwnProperty(key)) {
      const value = proxyObject[key];
      if (Array.isArray(value)) {
        // 数组递归处理
        normalObject[key] = value.map((item) => {
          if (typeof item === 'object' && item !== null) {
            return convertProxyToNormal(item);
          }
          return item;
        });
      } else if (typeof value === 'object' && value !== null) {
        // 普通对象递归处理
        normalObject[key] = convertProxyToNormal(value);
      } else {
        // 非对象直接赋值
        normalObject[key] = value;
      }
    }
  }
  return normalObject;
}

// 定义一个对象数组类型，其中每个对象的键值对可以是任意类型
type ObjectArray = { [key: string]: any }[];

/**
 * 将对象数组中的每个对象的字符串数组属性转换为逗号分隔的字符串
 * @param array 对象数组
 * @param unMergeColumn 不需要合并的属性名集合
 */
export function convertArrayValuesToStrings(
  array: ObjectArray,
  unMergeColumn?: string[]
): ObjectArray {
  return array.map((obj) => {
    // 遍历数组中的每个对象，将其属性值转换为字符串形式
    return convertObjectValuesToStrings(obj, unMergeColumn);
  });
}

/**
 * 将对象的属性值转换为字符串形式
 * @param obj 需要转换的对象
 * @param unMergeColumn 不需要合并的属性名集合
 */
function convertObjectValuesToStrings(
  obj: { [key: string]: any },
  unMergeColumn?: string[]
): {
  [key: string]: any;
} {
  // 如果没有指定不合并的属性名集合，则直接返回原对象
  if (!unMergeColumn) {
    return {
      ...obj
    };
  }
  const newObj: { [key: string]: any } = {};

  for (const key in obj) {
    // 如果当前属性名在不合并的属性名集合中，则直接复制该属性到新对象
    if (unMergeColumn.includes(key)) {
      newObj[key] = obj[key];
    } else if (key === 'children' && Array.isArray(obj[key])) {
      // 如果当前属性名为children且其值为数组，则递归处理数组中的每个对象
      newObj[key] = obj[key].map((item: any) =>
        convertObjectValuesToStrings(item)
      );
    } else if (Array.isArray(obj[key])) {
      // 如果当前属性值为数组，则将其转换为逗号分隔的字符串
      newObj[key] = obj[key].join(',');
    } else if (typeof obj[key] === 'object' && obj[key] !== null) {
      // 如果当前属性值为对象，则递归处理该对象
      newObj[key] = convertObjectValuesToStrings(obj[key]);
    } else {
      // 如果当前属性值为其他类型，则直接复制该属性到新对象
      newObj[key] = obj[key];
    }
  }

  return newObj;
}
