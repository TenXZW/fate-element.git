/**
 *  转换为px
 * @param value 值
 */
export function formatPx(value: string | number): string {
  return typeof value === 'number' ? `${value}px` : value;
}
