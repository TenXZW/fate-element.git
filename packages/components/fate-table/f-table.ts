// 导入Vue的App类型，用于后续的组件注册
import { App } from 'vue';

// 导入自定义的FTable组件
import FTable from './FForm.vue';

// 为FTable组件定义一个install方法，以便在Vue应用中注册该组件
FTable.install = (app: App) => {
  // 组件注册，按需引入
  // 这里将FTable组件注册到全局，使得在项目任何地方都可以使用<FTable />标签
  app.component('FTable', FTable);
  // 返回app实例，支持链式调用
  return app;
};

// 默认导出FTable组件，使得其他模块可以通过import语句导入它
export default FTable;
