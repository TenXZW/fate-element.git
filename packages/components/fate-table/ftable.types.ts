/**
 * FTable 组件的属性接口
 * 定义了表格组件所需的各种配置和数据
 */
export interface FTableProps {
  // 表格数据，数组类型，每个元素是一个对象代表一行数据
  tableData: Record<any, any>[];
  // 列配置，数组类型，每个元素是一个对象代表一列的配置
  columns: FFColumn[];
  // 是否处于加载中状态，默认为 false
  loading?: boolean;
  // 是否有选择框，默认为 false
  isSelection?: boolean;
  // 是否有索引列，默认为 false
  hasIndex?: boolean;
  // 底部样式，字符串类型，用于指定底部的样式
  footerStyle?: string;
  // 分页配置，可以是 FFPages 类型的对象或任意类型
  pages?: FFPages | any;
}

/**
 * 分页配置接口
 * 定义了分页组件所需的配置
 */
export interface FFPages {
  // 当前页码
  pageNum?: number;
  // 每页条数
  pageSize?: number;
  // 总数据数
  total?: number;
}

/**
 * 每一列的配置对象
 * 详细定义了表格中每一列的属性和行为
 */
export interface FFColumn {
  // el-table prop 属性 字段名
  prop: string;
  // el-table label 属性 列名
  label: string;
  // 判断是否为排序
  sortable?: boolean;
  // 判断是否隐藏，一些业务需求，比如不同的权限可能就不展示了
  hidden?: boolean;
  // 列无数据占位符
  emptyStr?: string;
  // el-table fixed 属性 左右固定布局  只能在头尾的配置中存在
  fixed?: 'left' | 'right' | boolean;
  // el-table width 属性 宽度
  width?: number;
  // 自定义渲染
  render?: any;
  isNotMergeArray?: boolean;
}
