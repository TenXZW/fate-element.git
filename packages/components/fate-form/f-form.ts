import { App } from 'vue';

import FForm from './FForm.vue';

/**
 * FForm组件的安装方法
 * @param app Vue应用实例
 * @returns 返回安装后的Vue应用实例
 */
FForm.install = (app: App) => {
  // 组件注册，按需引入
  app.component('FForm', FForm);
  return app;
};

export default FForm;
