// 导入FForm组件，用于表单的创建和管理
import FForm from '@fate/components/fate-form/f-form.ts';
// 导入FTable组件，用于表格的展示和操作
import FTable from '@fate/components/fate-table/f-table.ts';

// 默认导出一个包含FForm和FTable组件的数组，方便统一管理和引用
export default [FForm, FTable];

// 通过命名导出，使得FForm组件可以单独被引用
export { FForm };
// 通过命名导出，使得FTable组件可以单独被引用
export { FTable };
