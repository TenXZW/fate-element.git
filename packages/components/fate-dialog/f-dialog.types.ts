/**
 * 定义一个对话框选项的接口
 * 该接口用于配置对话框组件的参数
 */
export interface DialogOptions {
  // 对话框的标题，可选
  title?: string;
  // 对话框的内容，可选
  content?: string;
  // 可传递的 Vue 组件，用于在对话框中渲染自定义组件，可选
  component?: any;
  // 传递给组件的 props，键值对形式，可选
  props?: Record<string, any>;
  // 传递给弹窗的 props，键值对形式，可选
  options?: Record<string, any>;
}
