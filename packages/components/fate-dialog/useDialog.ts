import { DialogOptions } from '@fate/components/fate-dialog/f-dialog.types';
import FDialog from '@fate/components/fate-dialog/FDialog.vue';
import { createApp, h } from 'vue';

export function useDialog() {
  return (options: DialogOptions) => {
    let resolveFn: (value: any) => void;
    const promise = new Promise<any>((resolve) => {
      resolveFn = resolve;
    });

    const container = document.createElement('div');
    container.setAttribute('class', 'fate-dialog-container');
    document.body.appendChild(container);

    // 定义一个初始的 close 函数作为占位符
    let close = () => {
      console.warn('close function is not yet initialized');
    };

    const app = createApp({
      render() {
        // 在 app 中对 close 进行实际赋值
        close = (result?: any) => {
          resolveFn(result); // 解析 Promise 并传递参数
          app.unmount(); // 卸载组件
          if (container.parentNode) {
            container.parentNode.removeChild(container); // 删除容器
          }
        };

        return h(
          FDialog,
          {
            title: options.title || '',
            onClose: close,
            visible: true,
            ...options.options
          },
          {
            default: () => {
              if (options.component) {
                return h(options.component, {
                  ...options.props,
                  close // 将 close 方法作为 prop 传递给子组件
                });
              }
              return options.content || '';
            }
          }
        );
      }
    });

    app.mount(container);

    // 返回包含外部可调用的关闭方法和 Promise
    return {
      close, // 外部可用的关闭方法
      result: promise // 返回的 Promise，可用 .then()
    };
  };
}
